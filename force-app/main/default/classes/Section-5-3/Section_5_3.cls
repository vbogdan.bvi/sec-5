/**
 * @description       : 
 * @author            : Viktor Bogdan
 * @group             : 
 * @last modified on  : 06-28-2021
 * @last modified by  : Viktor Bogdan
 * Modifications Log 
 * Ver   Date         Author          Modification
 * 1.0   06-28-2021   Viktor Bogdan   Initial Version
**/
// Limits
public with sharing class Section_5_3 {
  /* 
    * Task 1
    * Что не так с этим кодом? Какой лимит может быть превышен и как можно этоисправить? Исправьте этот код.

      List<Contact> firstContacts = new List<Contact>();
      for (Integer i = 0; i < 200; i++) {
        firstContacts.add([SELECT Name FROM Contact LIMIT 1 OFFSET :i]);
      }

    *  System.QueryException: List has no rows for assignment to SObject
    *  Too many SOQL queries: 101         ---       Total number of SOQL queries issued1 100
  */

  public static void task1() {
    List<Contact> firstContacts = [SELECT Name FROM Contact LIMIT 200];
  }

  /*
    * Task 2
    * Что не так с этим кодом? Какой лимит может быть превышен и как можно этоисправить? Исправьте этот код.
        for (Integer i = 0; i < 200; i++) {
          Contact cnt = new Contact(lastName = 'test', firstName = 'test' + i);
          insert cnt;
        }

    * System.LimitException: Too many DML statements: 151      -  Total number of DML statements issued2 150

  */

  public static void task2() {
    List<Contact> contacts = new List<Contact>();
    for (Integer i = 0; i < 200; i++) {
      Contact cnt = new Contact(lastName = 'test', firstName = 'test' + i);
      contacts.add(cnt);
    }
    insert contacts;
  }
  /*
    * Task 3
    * Что не так с этим кодом? Какой лимит может быть превышен и как это исправить? Исправьте этот код.

      Set<String> allNames = new Set<String>();
        List<Contact> allContacts = [SELECT firstName FROM Contact];
        for (Contact contactItem : allContacts) {
          allNames.add(contactItem.firstName);
        }

    * Total number of records retrieved by SOQL queries  -----------	50,000
    * Maximum CPU time on the Salesforce servers5	 ----------- 10,000 milliseconds
  */

  public static void task3() {
    List<AggregateResult> allContacts = [SELECT firstName FROM Contact GROUP BY FirstName];
    Set<String> allNames = new Set<String>();
    for (AggregateResult aggr : allContacts) {
      allNames.add((String) aggr.get('firstName'));
    }
    system.debug('' + allNames.size() + ' -----  ' + allNames);
  }

  /*
    * Task 4
    * Ниже даны два апекс класса: триггер и хэндлер. Что не так с этим кодом (в плане риска превысить лимиты)? Какой лимит может быть превышен и как это исправить? 
    * Исправьте этот код.
    
    * Trigger:

      trigger agentSmithTrigger on Contact (before update) {
          for(Contact target: trigger.new) {
              agentSmithTriggerHandler.infect(target);
          }
      }
    
    * Handler:

        public with sharing class agentSmithTriggerHandler {
        public static void infect(Contact target) {
            Contact agentSmith = [SELECT Id FROM Contact WHERE ID = :target.infectedBy ;
          }
        }


      * Total number of SOQL queries issued1 100
    ---------------------------------------------------------------------------
      * should be like this
      * Trigger:

        trigger agentSmithTrigger on Contact (before update) {
                agentSmithTriggerHandler.infect(trigger.new);
        }
    
      * Handler:

        public with sharing class agentSmithTriggerHandler {
          public static void infect(List<Contact> targetList) {
            List<Contact> agentSmithList = [SELECT Id FROM Contact WHERE ID in :targetList];
          }
        }
  */
}
