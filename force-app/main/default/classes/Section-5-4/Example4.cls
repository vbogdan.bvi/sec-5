/**
 * @description       : 
 * @author            : Viktor Bogdan
 * @group             : 
 * @last modified on  : 06-28-2021
 * @last modified by  : Viktor Bogdan
 * Modifications Log 
 * Ver   Date         Author          Modification
 * 1.0   06-28-2021   Viktor Bogdan   Initial Version
**/
public class Example4 {
  // public Set<String> getUniqueContactNames() {
  //     List<SObject> sobjects 			= new List<SObject>();
  //     Set<String> uniqueContactNames 	= new Set<String>();

  //     try {
  //         sobjects 			= getRecords('Contact');
  //         uniqueContactNames 	= getUniqueRecordNames(sobjects);
  //     } catch(Exception ex) {
  //         system.debug('Unkown Error: ' + ex.getMessage());
  //     }

  //     return uniqueContactNames;
  // }

  // public Set<String> getUniqueAccountNames() {
  //     List<SObject> sobjects 			= new List<SObject>();
  //     Set<String> uniqueAccountNames 	= new Set<String>();

  //     try {
  //         sobjects 			= getRecords('Account');
  //         uniqueAccountNames 	= getUniqueRecordNames(sobjects);
  //     } catch(Exception ex) {
  //         system.debug('Unkown Error: ' + ex.getMessage());
  //     }

  //     return uniqueAccountNames;
  // }

  // private List<SObject> getRecords(String sObjectApiName) {
  //     List<SObject> sobjects 	= new List<SObject>();

  //     sobjects = Database.query('SELECT Id, Name FROM ' + sObjectApiName);

  //     return sobjects;
  // }

  // private Set<String> getUniqueRecordNames(List<SObject> sobjects) {
  //     Set<String> uniqueNames = new Set<String>();

  //     for(SObject sobj: sobjects) {
  //         uniqueNames.add(String.valueOf(sobj.get('Name')));
  //     }

  //     return uniqueNames;
  // }

  /**
   * Returns the set of unique names of diferent sobject
   *
   * @param {String} sObjectApiName
   * @return {Set<String>}
   *
   */
  private Set<String> getUniqueNames(String sObjectApiName) {
    String verifiedSObjectApiName = String.escapeSingleQuotes(sObjectApiName);
    Set<String> uniqueNames = new Set<String>();
    try {
      List<SObject> sobjects = new List<SObject>();

      sobjects = Database.query('SELECT Id, Name FROM ' + verifiedSObjectApiName);

      for (SObject sobj : sobjects) {
        uniqueNames.add(String.valueOf(sobj.get('Name')));
      }
    } catch (Exception ex) {
      system.debug('Unkown Error: ' + ex.getMessage());
    }

    return uniqueNames;
  }


}
