public class Example1 {
  // public List<String> method() {
  //       List<SObject> objs;
  //       objs = [select id, name, email, phone from contact];
  //       	List<String> strings;
  //       	integer size = objs.size();
  //       for(integer i = 0; i < size; i ++) {
  //           	boolean f = false;
  //           integer sizeNew = strings.size();
  //               for(integer j = 0; j < sizeNew; j ++) {
  //               if(objs[i].get('name') == strings[j]) {
  //               f = true;
  //               }
  //               }
  //               if(f != true) {
  //            strings.add(string.valueof(objs[i].get('name')));
  //         }
  //       }
  //       return strings;
  //   		}

  /**
   * Returns the set of unique names of Contacts
   *
   * @return {Set<String>}
   *
   */
  public Set<String> getUniqueContactNames() {
    Set<String> uniqueContactNames = new Set<String>();
    List<Contact> contacts = [SELECT ID, Name FROM Contact];

    for (Contact cont : contacts) {
      uniqueContactNames.add(cont.Name);
    }

    return uniqueContactNames;
  }
}
