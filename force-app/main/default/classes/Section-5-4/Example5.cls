/**
 * @description       : 
 * @author            : Viktor Bogdan
 * @group             : 
 * @last modified on  : 06-28-2021
 * @last modified by  : Viktor Bogdan
 * Modifications Log 
 * Ver   Date         Author          Modification
 * 1.0   06-28-2021   Viktor Bogdan   Initial Version
**/
public class Example5 {
  // public static void func() {
  //     list<contact> list1 = [select id,name,phone, email from contact];
  //     list<contact> list1new = new list<contact>();
  //     for(contact c: list1)
  //     {
  //     Boolean f = false;
  //         integer i = 0;
  //         for(i=0;i<list1new.size();i++)
  //         {
  //             if(c.Name == list1new[i].Name){f=true;}

  //         }
  //         if(f!=true)
  //       {
  //      list1new.add(c);
  //         }
  //     }
  //     delete list1new;
  //     list<lead> list2 = [select id,name,phone, email,title from Lead];
  //     list<lead> list2new = new list<lead>();
  //     for(lead c: list2)
  //     {
  //     Boolean f = false;
  //         integer i = 0;
  //         for(i=0;i<list2new.size();i++)
  //         {
  //             if(c.Name == list2new[i].Name){f=true;}

  //         }
  //         if(f!=true)
  //       {
  //      list2new.add(c);
  //         }
  //     }
  //     delete list2new;
  // }

  /**
   * Delete records of sObject with unique name
   *
   * @param {String} sObjectApiName
   *
   */
  public static void deleteRecordsWithUniqueName(String sObjectApiName) {
    try {
      String verifiedSObjectApiName = String.escapeSingleQuotes(sObjectApiName);

      List<SObject> sobjects = new List<SObject>();
      Map<String, SObject> sobjWithUniqueNames = new Map<String, SObject>();

      sobjects = Database.query('SELECT Id, Name FROM ' + verifiedSObjectApiName);

      for (SObject sobj : sobjects) {
        sobjWithUniqueNames.put(String.valueOf(sobj.get('Name')), sobj);
      }
      delete sobjWithUniqueNames.values();
    } catch (Exception ex) {
      system.debug('Unkown Error: ' + ex.getMessage());
    }
  }

  /**
   * Delete records of Contacts and Leads with unique name
   */
  public static void deleteContactsAndLeadsWithUniqueName() {
    deleteRecordsWithUniqueName('Contact');
    deleteRecordsWithUniqueName('Lead');
  }
}
