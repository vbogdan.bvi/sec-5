/**
 * @author            : Viktor Bogdan
 * @last modified on  : 06-25-2021
 * @last modified by  : Viktor Bogdan
 * Ver   Date         Author          Modification
 * 1.0   06-25-2021   Viktor Bogdan   Initial Version
 **/
public virtual class Type {
  protected String contactType;
  protected Double amountOfMoney;

  /**
   * @return String
   **/
  public String getContactType() {
    return contactType;
  }

  /**
   * @param contactType {String}
   **/
  public void setContactType(String contactType) {
    this.contactType = contactType;
  }

  /**
   * @param amountOfMoney {Double}
   **/
  public void setAmountOfMoney(Double amountOfMoney) {
    this.amountOfMoney = amountOfMoney;
  }

  /**
   * @return Double
   **/
  public Double getAmountOfMoney() {
    return amountOfMoney;
  }
}
