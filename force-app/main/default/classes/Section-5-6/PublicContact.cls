/**
 * @author            : Viktor Bogdan
 * @last modified on  : 06-25-2021
 * @last modified by  : Viktor Bogdan
 * Ver   Date         Author          Modification
 * 1.0   06-25-2021   Viktor Bogdan   Initial Version
 **/
public class PublicContact extends Type {
  private Integer volunteerNumber;
  private Contact contactRecord;

  /**
   * @param volunteerNumber {Integer}
   * @param contactRecord {Contact}
   **/
  public PublicContact(Integer volunteerNumber, Contact contactRecord) {
    this.volunteerNumber = volunteerNumber;
    this.contactRecord = contactRecord;
  }

  public PublicContact() {
  }

  /**
   * @return Contact
   **/
  public Contact getContactRecord() {
    return contactRecord;
  }

  /**
   * @return Integer
   **/
  public Integer getVolunteerNumber() {
    return volunteerNumber;
  }

  /**
   * @ override method "toString"
   * @return String
   **/
  public override String toString() {
    String result =
      'volunteerNumber = ' +
      volunteerNumber +
      ', contactType = ' +
      getContactType() +
      ', Contact(Name = ' +
      contactRecord.Name +
      ', Amount = ' +
      contactRecord.Amount__c +
      ', Account.Name = ' +
      contactRecord.Account.Name +
      ')';
    return result;
  }
}
