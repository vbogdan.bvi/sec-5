/**
 * @author            : Viktor Bogdan
 * @last modified on  : 06-28-2021
 * @last modified by  : Viktor Bogdan
 * Ver   Date         Author          Modification
 * 1.0   06-24-2021   Viktor Bogdan   Initial Version
 **/
@isTest
public class AccountSwitcherImplTest {
  @IsTest
  static void testGetRandNumber() {
    Integer num = AccountSwitcherImpl.getRandNumber();
    System.assertEquals(6, String.valueOf(num).length(), 'number must have 6 digits');
    System.assert(Math.mod(num, 2) == 0, 'the number is even');
  }

  @IsTest
  static void testContainsThreeIdenticalDigits() {
    System.assertEquals(
      false,
      AccountSwitcherImpl.containsThreeIdenticalDigits(325462),
      'the number "325462" does not contain 6 identical digits'
    );
    System.assertEquals(
      true,
      AccountSwitcherImpl.containsThreeIdenticalDigits(111258),
      'the number "111258" contains 6 identical digits'
    );
    System.assertEquals(
      true,
      AccountSwitcherImpl.containsThreeIdenticalDigits(121218),
      'the number "121218" contains 6 identical digits'
    );
  }

  @IsTest
  static void testGetAndSort() {
    Test.startTest();
    AccSwitcherTestDataFactory.addDataToDB();

    Test.stopTest();

    AccountSwitcherImpl.getAndSort();

    System.assertEquals(20, AccountSwitcherImpl.getAllContacts().size(), ' - size of list "allContacts"');
    Integer numOfPrivateContact = 0;
    Integer numOfPublicContact = 0;
    for (Type typeObj : AccountSwitcherImpl.getAllContacts()) {
      if (typeObj instanceof PublicContact) {
        numOfPublicContact++;
      } else if (typeObj instanceof PrivateContact) {
        numOfPrivateContact++;
      }
    }
    System.assertEquals(9, numOfPrivateContact, ' - number of PrivateContact in list "allContacts"');
    System.assertEquals(11, numOfPublicContact, ' - number of PublicContact in list "allContacts"');
  }

  @IsTest
  static void testSortAllcontactsToPrivateAndPublicList() {
    Test.startTest();
    AccSwitcherTestDataFactory.addDataToDB();

    Test.stopTest();
    AccountSwitcherImpl.getAndSort();
    System.assertEquals(0, AccountSwitcherImpl.getPrContactList().size(), 'size of prContactList in the beginning');
    System.assertEquals(0, AccountSwitcherImpl.getPublContactList().size(), 'size of publContactList in the beginning');
    AccountSwitcherImpl.sortAllcontactsToPrivateAndPublicList();
    System.assertEquals(9, AccountSwitcherImpl.getPrContactList().size(), 'size of prContactList after sorting');
    System.assertEquals(11, AccountSwitcherImpl.getPublContactList().size(), 'size of publContactList after sorting');
  }

  @IsTest
  static void testUpdateChangedContactsInOrg() {
    Test.startTest();
    AccSwitcherTestDataFactory.initDataResult();
    ID prContactID = AccSwitcherTestDataFactory.dataResult.getChangedPrivateContacts().get(2).getContactRecord().ID;
    ID publContactID = AccSwitcherTestDataFactory.dataResult.getChangedPublicContacts().get(1).getContactRecord().ID;
    AccSwitcherTestDataFactory.dataResult.getChangedPrivateContacts()
      .get(2)
      .getContactRecord()
      .LastName = 'PrivateLastName';
    AccSwitcherTestDataFactory.dataResult.getChangedPublicContacts()
      .get(1)
      .getContactRecord()
      .LastName = 'PublicLastName';

    AccountSwitcherImpl.updateChangedContactsInOrg(AccSwitcherTestDataFactory.dataResult);
    Contact prContact = [SELECT id, LastName FROM Contact WHERE ID = :prContactID];
    Contact publContact = [SELECT id, LastName FROM Contact WHERE ID = :publContactID];
    System.assertEquals('PrivateLastName', prContact.LastName, ' prContact.LastName = "PrivateLastName"');
    System.assertEquals('PublicLastName', publContact.LastName, ' publContact.LastName = "PublicLastName"');

    Test.stopTest();
  }

  @IsTest
  static void testSwitchAccount() {
    Test.startTest();
    AccSwitcherTestDataFactory.addDataToDB();
    AccountSwitcherImpl.getAndSort();
    ID idCon = AccSwitcherTestDataFactory.contactList.get(0).ID;
    ID idConAcct = AccSwitcherTestDataFactory.contactList.get(0).AccountId;

    AccountSwitcherImpl.DataResult dataResult = AccountSwitcherImpl.switchAccount();
    Test.stopTest();
    System.assertEquals(20, AccountSwitcherImpl.getAllContacts().size(), 'size of allContact list');
    System.assertEquals(9, AccountSwitcherImpl.getPrContactList().size(), 'size of prContactList list');
    System.assertEquals(11, AccountSwitcherImpl.getPublContactList().size(), 'size of publContactList list');

    System.assert(
      dataResult.getChangedPrivateContacts().size() > 0,
      'there are some element in ChangedPrivateContacts list'
    );
    System.assert(
      dataResult.getChangedPublicContacts().size() > 0,
      'there are some element in  ChangedPublicContacts list'
    );
    Contact con = [SELECT id, Name, AccountId FROM Contact WHERE ID = :idCon];
    System.assertNotEquals(idConAcct, con.AccountId, 'must be different AccountID' + con.Name + '       ' + con.ID);
  }
}
