public interface AccountSwitcher {
  void getAndSort();
  AccountSwitcherImpl.DataResult switchAccount();
}
