/**
 * @author            : Viktor Bogdan
 * @last modified on  : 06-25-2021
 * @last modified by  : Viktor Bogdan
 * Ver   Date         Author          Modification
 * 1.0   06-25-2021   Viktor Bogdan   Initial Version
 **/
@isTest
private class PrivateContactTest {
  @isTest
  private static void testGetContactType() {
    System.assertEquals(
      AccSwitcherTestDataFactory.prContact.getContactType(),
      'testPrivateType',
      'ContactType = "testPrivateType"'
    );
  }
  @isTest
  private static void testGetContactRecord() {
    System.assertEquals(
      AccSwitcherTestDataFactory.prContact.getContactRecord(),
      AccSwitcherTestDataFactory.testContact,
      ' the contact is the same'
    );
  }

  @isTest
  private static void getCardNumber() {
    System.assertEquals(AccSwitcherTestDataFactory.prContact.getCardNumber(), 123456, ' CardNumber = 123456');
  }

  @isTest
  private static void testToString() {
    System.assertEquals(
      AccSwitcherTestDataFactory.prContact.toString(),
      'cardNumber = 123456, contactType = testPrivateType, Contact(Name = null, Amount = 265987, Account.Name = null)'
    );
  }
}
