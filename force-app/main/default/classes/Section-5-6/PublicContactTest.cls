@isTest
public class PublicContactTest {
  @isTest
  private static void testGetContactRecord() {
    System.assertEquals(
      AccSwitcherTestDataFactory.publicContact.getContactRecord(),
      AccSwitcherTestDataFactory.testContact
    );
  }

  @isTest
  private static void testVolunteerNumber() {
    System.assertEquals(
      AccSwitcherTestDataFactory.publicContact.getVolunteerNumber(),
      256898
    );
  }

  @isTest
  private static void testToString() {
    System.assertEquals(
      AccSwitcherTestDataFactory.publicContact.toString(),
      'volunteerNumber = 256898, contactType = testPublicType, Contact(Name = null, Amount = 265987, Account.Name = null)'
    );
  }
}
