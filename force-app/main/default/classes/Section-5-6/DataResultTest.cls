/**
 * @author            : Viktor Bogdan
 * @last modified on  : 06-25-2021
 * @last modified by  : Viktor Bogdan
 * Ver   Date         Author          Modification
 * 1.0   06-25-2021   Viktor Bogdan   Initial Version
 **/
@isTest
private class DataResultTest {
  @isTest
  private static void testGetChangedPrivateContacts() {
    Test.startTest();
    AccSwitcherTestDataFactory.initDataResult();
    System.assertEquals(
      4,
      AccSwitcherTestDataFactory.dataResult.getChangedPrivateContacts().size(),
      ' size of ChangedPrivateContacts list must be 4'
    );
    Test.stopTest();
  }

  @isTest
  private static void testGetChangedPublicContacts() {
    Test.startTest();
    AccSwitcherTestDataFactory.initDataResult();

    System.assertEquals(
      6,
      AccSwitcherTestDataFactory.dataResult.getChangedPublicContacts().size(),
      '  size of ChangedPublicContacts list must be 6'
    );
    Test.stopTest();
  }

  @isTest
  private static void testGetNotChangedPrivateContacts() {
    Test.startTest();
    AccSwitcherTestDataFactory.initDataResult();

    System.assertEquals(
      5,
      AccSwitcherTestDataFactory.dataResult.getNotChangedPrivateContacts().size(),
      ' size of NotChangedPrivateContacts list must be 5'
    );
    Test.stopTest();
  }

  @isTest
  private static void testAddChangedPrivateContact() {
    Test.startTest();
    AccSwitcherTestDataFactory.initDataResult();

    AccSwitcherTestDataFactory.dataResult.addChangedPrivateContact(new PrivateContact());
    System.assertEquals(
      5,
      AccSwitcherTestDataFactory.dataResult.getChangedPrivateContacts().size(),
      ' size of ChangedPrivateContacts list must be 5'
    );
    Test.stopTest();
  }

  @isTest
  private static void testAddNonChangedPrivateContact() {
    Test.startTest();
    AccSwitcherTestDataFactory.initDataResult();

    AccSwitcherTestDataFactory.dataResult.addNonChangedPrivateContact(new PrivateContact());
    System.assertEquals(
      6,
      AccSwitcherTestDataFactory.dataResult.getNotChangedPrivateContacts().size(),
      ' size of NotChangedPrivateContacts list must be 6'
    );
    Test.stopTest();
  }

  @isTest
  private static void testAddChangedPublicContact() {
    Test.startTest();
    AccSwitcherTestDataFactory.initDataResult();

    AccSwitcherTestDataFactory.dataResult.addChangedPublicContact(new PublicContact());
    System.assertEquals(
      7,
      AccSwitcherTestDataFactory.dataResult.getChangedPublicContacts().size(),
      ' size of ChangedPublicContacts list must be 7'
    );
    Test.stopTest();
  }
}
