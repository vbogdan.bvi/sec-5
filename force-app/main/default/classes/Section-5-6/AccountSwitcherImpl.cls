/**
 * @author            : Viktor Bogdan
 * @last modified on  : 06-28-2021
 * @last modified by  : Viktor Bogdan
 * Ver   Date         Author          Modification
 * 1.0   06-24-2021   Viktor Bogdan   Initial Version
 **/
public without sharing class AccountSwitcherImpl implements AccountSwitcher {
  private static List<Type> allContacts = new List<Type>();
  private static List<PrivateContact> prContactList = new List<PrivateContact>();
  private static List<PublicContact> publContactList = new List<PublicContact>();

  /**
   * @ return list of Type sobject
   * @return List<Type>
   **/
  public static List<Type> getAllContacts() {
    return allContacts;
  }

  /**
   * @ return list of PrivateContact sobject
   * @return List<PrivateContact>
   **/
  public static List<PrivateContact> getPrContactList() {
    return prContactList;
  }

  /**
   * @ return list of PublicContact sobject
   * @return List<PublicContact>
   **/
  public static List<PublicContact> getPublContactList() {
    return publContactList;
  }

  /*
    Метод getAndSort получает все контакты на орге 
    и если поле Amount у контакта больше 10000 то создать объект класса Private Contact Class 
    и заполнить у него поля ContactRecord - текущий контакт и 
    CardNumber - рандомное 6-значное число 
    (Если в этом числе есть хотя бы 3 одинаковые цифры, то в значение поля ContactType будет
    равно ‘Premier’, иначе ContactType =’Person’). 
    
    Иначе для текущего контакта создать
    объект Public Contact Class и заполнить у него поля ContactRecord - текущий контакт и
    VolunteerNumber - рандомное четное 6-значное число. Для всех объектов класса Public
    Contact, значение поля ContactType =’Person’. 
    Все созданные объекты метод ложит в статическое поле AllContacts. 
  */

  /**
   * @ get all Contacts from the org, create some Public Contact and Private Contact and put it to the list
   **/
  public static void getAndSort() {
    try {
      List<Contact> contacts = [SELECT Id, Name, Amount__c, Account.Name, AccountId FROM Contact];

      for (Contact con : contacts) {
        if (con.Amount__c > 10000) {
          Integer cardNumber = Integer.valueOf(Math.random() * 1000000);
          PrivateContact prContact = new PrivateContact(cardNumber, con);
          prContact.setAmountOfMoney(Double.valueOf(con.Amount__c));
          // if there are three identical digits in the cardNumber then "Premier" else "Person"
          if (containsThreeIdenticalDigits(cardNumber)) {
            prContact.setContactType('Premier');
          } else {
            prContact.setContactType('Person');
          }
          allContacts.add(prContact);
        } else {
          Integer volunteerNumber = getRandNumber();
          PublicContact publContact = new PublicContact(volunteerNumber, con);
          publContact.setAmountOfMoney(Double.valueOf(con.Amount__c));
          publContact.setContactType('Person');
          allContacts.add(publContact);
        }
      }
      System.debug('inside getAndSort method');
      System.debug('allContacts.size()  - ' + allContacts.size());
    } catch (Exception e) {
      System.debug('Line number  - ' + e.getLineNumber() + ', message - ' + e.getMessage());
    }
  }

  /*
    Метод switchAccount перебирает лист AllContacts. Он пробегает по полю AllContacts и
    формирует два листа (  List<Private Contact Class> и       List<Public Contact Class>) с
    соответствующими объектами. 

    После этого он меняет местами Account через поле ContactRecord у соответствующих объектов из листов. 
    Например первый элемент листа List<Private Contact Class> меняется Аккаунтом с первым значением листа
    List<Public Contact Class> и тд. 
    Если у элемента листа List<Private Contact Class> значение поля ContactType = ‘Premier’, то у этого объекта нельзя менять Аккаунт и он
    пропускается. 

    Далее все Contacts у которых был изменен Account обновляются(Update). 

    Метод возвращает объект внутреннего(inner) класса DataResult
    с 3-мя заполненными листами - ChangedPrivateContacts - лист со всеми измененными
    Private Contacts, ChangedPublicContacts - лист со всеми измененными PublicContacts и
    NonChangedPrivateContacts - лист со всеми не измененными PrivateContacts (те у
    которых ContactType = ‘Premier’)
  */

  /**
   * @ Sort list of Type Sobject to Public Contact list and Private Contact List. Make some kind of logic.
   * Update changed contacts and return DataResult object.
   * @author Viktor Bogdan | 06-24-2021
   * @return AccountSwitcherImpl.DataResult
   **/
  public static AccountSwitcherImpl.DataResult switchAccount() {
    AccountSwitcherImpl.DataResult dataResult = new AccountSwitcherImpl.DataResult();
    try {
      sortAllcontactsToPrivateAndPublicList();
      Integer i = 0;
      Integer j = 0;
      System.debug('inside switchAccount method, before while');
      while (i < prContactList.size() && j < publContactList.size()) {
        System.debug(' i = ' + i + '     j = ' + j);
        System.debug(' prContactList.get(' + i + ') = ' + prContactList.get(i));
        System.debug(' publContactList.get(' + j + ') = ' + publContactList.get(j));

        // if PrivateContact has type "Premier", than save this contact to list "NotChangedPrivateContacts"
        if (prContactList.get(i).getContactType() == 'Premier') {
          dataResult.addNonChangedPrivateContact(prContactList.get(i));
          System.debug(
            ' ----  PrivateContact has type "Premier"  ----- NotChangedPrivateContacts.add  = ' + prContactList.get(i)
          );
          i++;

          continue;
        }
        replaceAccountInContacts(prContactList.get(i).getContactRecord(), publContactList.get(j).getContactRecord());

        dataResult.addChangedPrivateContact(prContactList.get(i));
        dataResult.addChangedPublicContact(publContactList.get(j));

        System.debug('ChangedPrivateContacts.size  = ' + dataResult.getChangedPrivateContacts().size());
        System.debug('getChangedPublicContacts.size  = ' + dataResult.getChangedPublicContacts().size());

        i++;
        j++;
      }
      System.debug('nonChangedPrivateContacts.size  = ' + dataResult.getNotChangedPrivateContacts().size());

      updateChangedContactsInOrg(dataResult);
    } catch (Exception e) {
      System.debug('Line number - ' + e.getLineNumber() + ', message - ' + e.getMessage());
    }
    return dataResult;
  }

  /**
   * @ replace account in two contacts
   * @author Viktor Bogdan | 06-25-2021
   * @param con1 {Contact}
   * @param con2 {Contact}
   **/
  @TestVisible
  private static void replaceAccountInContacts(Contact con1, Contact con2) {
    System.debug('con1.AccountId = ' + con1.AccountId + ',  con2.AccountId =  ' + con2.AccountId);
    Id tempID = con1.AccountId;
    con1.AccountId = con2.AccountId;
    con2.AccountId = tempID;
  }

  /**
   * @ sorts all contacts to PrivateContactList and PublicContactList
   * @author Viktor Bogdan | 06-24-2021
   **/
  @TestVisible
  private static void sortAllcontactsToPrivateAndPublicList() {
    prContactList = new List<PrivateContact>();
    publContactList = new List<PublicContact>();
    for (Type cont : allContacts) {
      if (cont instanceof PublicContact) {
        publContactList.add((PublicContact) cont);
        System.debug('public contact -   ' + cont);
      } else if (cont instanceof PrivateContact) {
        prContactList.add((PrivateContact) cont);
        System.debug('private contact -   ' + cont);
      }
    }
    System.debug('inside sortAllcontactsToPrivateAndPublicList method');
    System.debug('prContactList.size()  - ' + prContactList.size());
    System.debug('publContactList.size()  - ' + publContactList.size());
  }

  /**
   * @update all changed Contacts in orgdescription
   * @author Viktor Bogdan | 06-24-2021
   * @param {AccountSwitcherImpl.DataResult} dataResult
   */
  @TestVisible
  private static void updateChangedContactsInOrg(AccountSwitcherImpl.DataResult dataResult) {
    List<Contact> contacts = new List<Contact>();
    for (PrivateContact con : dataResult.getChangedPrivateContacts()) {
      contacts.add(con.getContactRecord());
    }

    for (PublicContact con : dataResult.getChangedPublicContacts()) {
      contacts.add(con.getContactRecord());
    }

    update contacts;
  }

  /**
   * Return "true" if number contains 3 or more the same digits and "false" if not
   *
   * @param {Integer} numb
   * @return {Boolean}
   *
   */
  @TestVisible
  private static Boolean containsThreeIdenticalDigits(Integer numb) {
    String numToStr = String.valueOf(numb);
    for (Integer i = 0; i < numToStr.length() - 2; i++) {
      if (numToStr.countMatches(numToStr.substring(i, i + 1)) >= 3) {
        return true;
      }
    }
    return false;
  }

  /**
   * @get random even six-digit number
   * @author Viktor Bogdan | 06-24-2021
   * @return Integer
   **/
  @TestVisible
  private static Integer getRandNumber() {
    Integer result = Integer.valueOf(Math.random() * 1000000);
    if (Math.mod(result, 2) == 0) {
      return result;
    } else if (result == 0) {
      return 10000;
    } else {
      return result - 1;
    }
  }

  public class DataResult {
    private List<PrivateContact> changedPrivateContacts = new List<PrivateContact>();
    private List<PublicContact> changedPublicContacts = new List<PublicContact>();
    private List<PrivateContact> nonChangedPrivateContacts = new List<PrivateContact>();

    /**
     * @return List<PrivateContact>
     **/
    public List<PrivateContact> getChangedPrivateContacts() {
      return changedPrivateContacts;
    }

    /**
     * @return List<PublicContact>
     **/
    public List<PublicContact> getChangedPublicContacts() {
      return changedPublicContacts;
    }
    /**
     * @return List<PrivateContact>
     **/
    public List<PrivateContact> getNotChangedPrivateContacts() {
      return nonChangedPrivateContacts;
    }

    /**
     * @ add this contact to changedPrivateContacts list
     * @param prContact {PrivateContact}
     **/
    public void addChangedPrivateContact(PrivateContact prContact) {
      changedPrivateContacts.add(prContact);
    }

    /**
     * @ add this contact to nonChangedPrivateContacts list
     * @param prContact {PrivateContact}
     **/
    public void addNonChangedPrivateContact(PrivateContact prContact) {
      nonChangedPrivateContacts.add(prContact);
    }

    /**
     * @ add this contact to changedPublicContacts list
     * @param publContact {PublicContact}
     **/
    public void addChangedPublicContact(PublicContact publContact) {
      changedPublicContacts.add(publContact);
    }
  }
}
