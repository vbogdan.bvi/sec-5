/**
 * @ data factory class fot tests
 * @author            : Viktor Bogdan
 * @last modified on  : 06-28-2021
 * @last modified by  : Viktor Bogdan
 * Ver   Date         Author          Modification
 * 1.0   06-25-2021   Viktor Bogdan   Initial Version
 **/
@isTest
public with sharing class AccSwitcherTestDataFactory {
  public static Type typeObject;
  public static PrivateContact prContact;
  public static PublicContact publicContact;
  public static Contact testContact;
  public static AccountSwitcherImpl.DataResult dataResult;
  public static AccountSwitcherImpl accSwitcher;
  public static List<Contact> contactList;
  static {
    typeObject = new Type();
    typeObject.setAmountOfMoney(12526);
    typeObject.setContactType('testContactType');

    testContact = new Contact(FirstName = 'testFirstName', LastName = 'testLastName', Amount__c = 265987);

    prContact = new PrivateContact(123456, testContact);
    prContact.setAmountOfMoney(26598);
    prContact.setContactType('testPrivateType');

    publicContact = new PublicContact(256898, testContact);
    publicContact.setAmountOfMoney(365658);
    publicContact.setContactType('testPublicType');

    accSwitcher = new AccountSwitcherImpl();
  }

  public static void initDataResult() {
    dataResult = new AccountSwitcherImpl.DataResult();
    addDataToDB();
    Integer i = 0;
    for (Integer j = 0; j < 4; j++) {
      dataResult.addChangedPrivateContact(new PrivateContact(100000 + 1 * 10, contactList.get(i)));
      dataResult.addNonChangedPrivateContact(new PrivateContact(200000 + 1 * 10, contactList.get(i + 1)));
      dataResult.addChangedPublicContact(new PublicContact(300000 + 1 * 10, contactList.get(i + 2)));
      i = i + 3;
    }
    dataResult.addNonChangedPrivateContact(new PrivateContact(123456, contactList.get(15)));
    dataResult.addChangedPublicContact(new PublicContact(423456, contactList.get(16)));
    dataResult.addChangedPublicContact(new PublicContact(623456, contactList.get(17)));
  }

  public static void addDataToDB() {
    contactList = new List<Contact>();
    List<Account> accountList = new List<Account>();
    for (Integer i = 0; i < 20; i++) {
      accountList.add(new Account(Name = 'Account ' + i));
    }
    insert accountList;

    for (Integer i = 0; i < 20; i++) {
      Contact con = new Contact(
        FirstName = 'fName ' + i,
        LastName = 'lName ' + i,
        AccountId = accountList.get(i).ID,
        Amount__c = 9000 + i * 100
      );
      contactList.add(con);
    }
    insert contactList;
  }
}
