/**
 * @description       : 
 * @author            : Viktor Bogdan
 * @group             : 
 * @last modified on  : 06-25-2021
 * @last modified by  : Viktor Bogdan
 * Modifications Log 
 * Ver   Date         Author          Modification
 * 1.0   06-25-2021   Viktor Bogdan   Initial Version
**/
@isTest
public class TypeTest {
  @IsTest
  static void testGetContactType() {
    System.assertEquals(
      AccSwitcherTestDataFactory.typeObject.getContactType(),
      'testContactType',
      ' ContactType = "testContactType"'
    );
  }

  @IsTest
  static void testGetAmountOfMoney() {
    System.assertEquals(AccSwitcherTestDataFactory.typeObject.getAmountOfMoney(), 12526, 'amountOfMoney = 12526');
  }

  @IsTest
  static void testSetContactType() {
    Type typeObj = new Type();
    typeObj.setContactType('testContactType');
    System.assertEquals(typeObj.getContactType(), 'testContactType', ' ContactType = "testContactType"');
  }

  @IsTest
  static void testSetAmmountOfMoney() {
    Type typeObj = new Type();
    typeObj.setAmountOfMoney(256898);
    System.assertEquals(typeObj.getAmountOfMoney(), 256898, 'amountOfMoney = 256898');
  }
}
