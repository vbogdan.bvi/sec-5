/**
 * @description       :
 * @author            : Viktor Bogdan
 * @last modified on  : 06-25-2021
 * @last modified by  : Viktor Bogdan
 * 1.0   06-25-2021   Viktor Bogdan   Initial Version
 **/
public class PrivateContact extends Type {
  private Integer cardNumber;
  private Contact contactRecord;

  /**
   * @ constructor of PrivateContact class
   * @param cardNumber
   * @param contactRecord
   **/
  public PrivateContact(Integer cardNumber, Contact contactRecord) {
    this.cardNumber = cardNumber;
    this.contactRecord = contactRecord;
  }

  public PrivateContact() {
  }

  /**
   * @return Contact
   **/
  public Contact getContactRecord() {
    return contactRecord;
  }

  public Integer getCardNumber() {
    return cardNumber;
  }

  /**
   * @ override method "toString"
   * @return String
   **/
  public override String toString() {
    String result =
      'cardNumber = ' +
      cardNumber +
      ', contactType = ' +
      getContactType() +
      ', Contact(Name = ' +
      contactRecord.Name +
      ', Amount = ' +
      contactRecord.Amount__c +
      ', Account.Name = ' +
      contactRecord.Account.Name +
      ')';
    return result;
  }
}
