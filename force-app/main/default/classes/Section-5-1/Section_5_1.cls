/**
 * @description       :
 * @author            : Viktor Bogdan
 * @group             :
 * @last modified on  : 06-28-2021
 * @last modified by  : Viktor Bogdan
 * Modifications Log
 * Ver   Date         Author          Modification
 * 1.0   06-25-2021   Viktor Bogdan   Initial Version
 **/
public with sharing class Section_5_1 {
  // 1)Написать метод класса, в котором будут несколько блоков try, вкаждом из которых создаются и обрабатываются исключения следующих типов:
  // sObject exception, DML exception, List exception, NullPointer exeption.
  public static void catchExeption() {
    try {
      //   Integer i = 2 / 0;
      Contact con = new Contact();
      insert con;

      List<Integer> list1 = new List<Integer>();
      list1.get(2);
    } catch (SObjectException e) {
      System.debug('The SObjectException has occurred: ' + e.getMessage());
    } catch (DmlException e) {
      System.debug('The DmlException has occurred: ' + e.getMessage());
    } catch (ListException e) {
      System.debug('The ListException has occurred: ' + e.getMessage());
    } catch (NullPointerException e) {
      System.debug('The NullPointerException has occurred: ' + e.getMessage());
    } catch (Exception e) {
      System.debug('The Exception has occurred: ' + e.getMessage());
    }
  }

  // 2) Написать метод класса. В нем будет блок try, в котором надо сгенерировать DML exception и обработать его, в блоке catch нужно использовать все
  // дополнительные методы исключения, уникальные для DML exception.
  public static void catchDMLExeption() {
    try {
      list<Contact> contacts = new List<Contact>();
      for (Integer i = 0; i < 10; i++) {
        Contact con = new Contact(FirstName = 'test111', LastName = 'testLastName');
        contacts.add(con);
      }
      Contact con = new Contact(LastName = 'testLastName');
      contacts.add(con);
      insert contacts;
    } catch (DmlException e) {
      System.debug('The DmlException has occurred: ' + e.getMessage());
      System.debug('The DmlException method "getDmlFieldNames": ' + e.getDmlFieldNames(0));
      System.debug('The DmlException method "getDmlFields": ' + e.getDmlFields(0));
      System.debug('The DmlException method "getDmlId": ' + e.getDmlId(0));
      System.debug('The DmlException method "getDmlIndex": ' + e.getDmlIndex(0));
      System.debug('The DmlException method "getDmlMessage": ' + e.getDmlMessage(0));
      System.debug('The DmlException method "getDmlStatusCode": ' + e.getDmlStatusCode(0));
      System.debug('The DmlException method "getDmlType": ' + e.getDmlType(0));
      System.debug('The DmlException method "getNumDml": ' + e.getNumDml());
    }
  }

  // 3) Написать метод класса, который принимает Аккаунты. Если у них поле ​NumberOfEmployees не заполнено, то генерировать пользовательское исключение и обработать его.
  public static void addAccounts(List<Account> accounts) {
    try {
      for (Account acc : accounts) {
        if (acc.NumberOfEmployees == null) {
          throw new CustomException('NumberOfEmployees is null. Account name is ----- ' + acc.Name);
        }
      }
    } catch (CustomException e) {
      system.debug('The CustomException has occurred: ' + e.getMessage());
    } catch (Exception e) {
      System.debug('The Exception has occurred: ' + e.getMessage());
    }
  }

  public class CustomException extends Exception {
  }
}
