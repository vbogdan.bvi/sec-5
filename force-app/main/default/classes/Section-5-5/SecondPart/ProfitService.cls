public with sharing class ProfitService {
  /**
   * 3. Написать иннер класс для объекта ​Contact​, который реализует ранее созданный интерфейс
   */
  public class ContactImpl implements ProfitInterface {
    private String firstName;
    private String lastName;
    private Decimal profit;

    public ContactImpl(String fName, String lName, Decimal profit) {
      firstName = fName;
      lastName = lName;
      this.profit = profit;
    }

    public Decimal calcProfit() {
      return profit;
    }
  }

  /**
   * 4. Написать иннер класс для объекта ​Account​, который реализует ранее созданный интерфейс,
   * данный класс обязательно должен содержать всебе список записей иннер класса всех его контактов
   */
  public class AccountImpl implements ProfitInterface {
    private String name;
    private List<ContactImpl> contacts;

    public AccountImpl(String name, List<ContactImpl> contacts) {
      this.name = name;
      this.contacts = contacts;
    }
    public Decimal calcProfit() {
      Decimal result = 0;
      for (Integer i = 0; i < contacts.size(); i++) {
        result = result + contacts.get(i).calcProfit();
      }
      return result;
    }
  }

  /**
   * 6. Реализовать сервис класс, с методом, который принимает список иннер классов (аккаунта или контактов) и возвращает суммарный доход,
   * это обязательно должен быть 1 метод, работающий с обоими иннер классами
   *
   *  */
  public static Decimal calcSumProfit(List<ProfitInterface> acctsAndContacts) {
    Decimal result = 0;
    for (ProfitInterface var : acctsAndContacts) {
      result = result + var.calcProfit();
    }
    return result;
  }

  /**
   * 7. Реализовать метод, который получает с орга список всех аккаунтов и их контактов, оборачивает их в иннер класс и возвращает список экземпляров
   */

  public static List<AccountImpl> getAccountsFromOrg() {
    List<Account> acctsList = [
      SELECT Name, Id, (SELECT Id, FirstName, LastName, Profit__c FROM Contacts)
      FROM Account
    ];
    List<AccountImpl> acctsImplList = new List<AccountImpl>();
    for (Account acct : acctsList) {
      List<ContactImpl> contactsImplList = new List<ContactImpl>();

      for (Contact con : acct.Contacts) {
        ContactImpl conImpl = new ContactImpl(
          con.FirstName,
          con.LastName,
          con.Profit__c
        );
        contactsImplList.add(conImpl);
      }
      AccountImpl acctImpl = new AccountImpl(acct.Name, contactsImplList);
      acctsImplList.add(acctImpl);
    }
    return acctsImplList;
  }

  /**
   * 8. Разработать метод, который получает с орга списоквсех контактов, оборачивает их в иннер класс и возвращает список экземпляров
   */

  public static List<ContactImpl> getContactsFromOrg() {
    List<Contact> contacts = [
      SELECT ID, FirstName, LastName, Profit__c
      FROM Contact
    ];
    List<ContactImpl> contactsImplList = new List<ContactImpl>();
    for (Contact con : contacts) {
      ContactImpl conImpl = new ContactImpl(
        con.FirstName,
        con.LastName,
        con.Profit__c
      );
      contactsImplList.add(conImpl);
    }
    return contactsImplList;
  }

  /*
  для запуска в anonymous

  
  for (ProfitService.ContactImpl var : ProfitService.getContactsFromOrg()) {
        System.debug(var);
    }

    for (ProfitService.AccountImpl var : ProfitService.getAccountsFromOrg()) {
        System.debug(var);
    }

    system.debug(ProfitService.calcSumProfit(ProfitService.getContactsFromOrg()));
    system.debug(ProfitService.calcSumProfit(ProfitService.getAccountsFromOrg()));


    System.debug([SELECT sum(Profit__c) summ FROM Contact]);
  
  
  */
}
