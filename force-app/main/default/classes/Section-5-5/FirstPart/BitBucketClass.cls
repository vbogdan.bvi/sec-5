public with sharing class BitBucketClass implements Vcs {
    public static void push() {
        System.debug('pushed to BitBucket');
    }
    public static void pull() {
      System.debug('pulled from BitBucket');
  
    }
    public static void methodCommit(String comment) {
        System.debug('commit to BitBucket with comment - '+ comment);
    }
}
