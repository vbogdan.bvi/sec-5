public with sharing class VcsServiceClass {
  private static List<Vcs> vcSystems = new List<Vcs>{
    new GitLabClass(),
    new BitBucketClass(),
    new GitLabClass()
  };

  public static void push(Vcs controlSystem) {
    controlSystem.push();
  }
  public static void pull(Vcs controlSystem) {
    controlSystem.pull();
  }
  public static void methodCommit(Vcs controlSystem, String comment) {
    controlSystem.methodCommit(comment);
  }

  public static void testPaternStrategy() {
    for (Integer i = 0; i < vcSystems.size(); i++) {
      push(vcSystems.get(i));
      pull(vcSystems.get(i));
      methodCommit(vcSystems.get(i), 'test - ' + i);
    }
  }
}
