public with sharing class GitLabClass implements Vcs {
  public static void push() {
    System.debug('pushed to GitLab');
  }
  public static void pull() {
    System.debug('pulled from GitLab');
  }
  public static void methodCommit(String comment) {
    System.debug('commit to GitLab with comment - ' + comment);
  }
}
