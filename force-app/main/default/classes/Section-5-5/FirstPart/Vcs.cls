global interface Vcs {
  void push();
  void pull();
  void methodCommit(String comment);
}
