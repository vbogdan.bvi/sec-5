/**
 * @description       :
 * @author            : Viktor Bogdan
 * @group             :
 * @last modified on  : 06-25-2021
 * @last modified by  : Viktor Bogdan
 * Modifications Log
 * Ver   Date         Author          Modification
 * 1.0   06-25-2021   Viktor Bogdan   Initial Version
 **/
@isTest
public class CrowCalcTest {
  @isTest
  public static void testAddCrows() {
    Integer initialCrows = [SELECT COUNT() FROM Crow__c];
    Boolean addCrowsResult = CrowCalc.addCrows(10);
    Integer crowsAfter = [SELECT COUNT() FROM Crow__c] - initialCrows;
    System.assert(crowsAfter == 10, 'Wrong answer!');
    System.assert(addCrowsResult, 'Success addition not "true"');
  }

  @isTest
  public static void testSubtractCrows() {
    Integer initialCrows = [SELECT COUNT() FROM Crow__c];
    Boolean subtractCrowsResult = CrowCalc.subtractCrows(5);
    Integer crowsAfter = initialCrows - [SELECT COUNT() FROM Crow__c];
    System.assert(crowsAfter == 5, 'Wrong answer!');
    System.assert(subtractCrowsResult, 'Success addition not "true"');
  }

  @isTest
  public static void testGetTotal() {
    Integer totalCount = [SELECT COUNT() FROM Crow__c];
    System.assert(totalCount == CrowCalc.getTotal(), 'Method "getTotal" is working!');
  }

  @isTest
  public static void testResetCalc() {
    Boolean resetCalcBool = CrowCalc.resetCalc();
    Integer totalCount = [SELECT COUNT() FROM Crow__c];
    System.assert(resetCalcBool, 'Method "resetCalc" is working!');
    System.assert(totalCount == 0, 'Method "resetCalc" is working!');
  }

  @TestSetup
  static void makeData() {
    CrowCalc.addCrows(20);
  }

  @isTest
  public static void testAddCrowsWithNull() {
    Boolean addCrowsResult = CrowCalc.addCrows(null);
    System.assertNotEquals(addCrowsResult, true, 'addCrows return false if argument = null');
  }

  @isTest
  public static void testAddCrowsWithNegativeNumber() {
    Boolean addCrowsResult = CrowCalc.addCrows(-5);
    System.assertEquals(addCrowsResult, true, 'addCrows return false if argument negative number');
  }

  @isTest
  public static void testSubtractCrowsWithNull() {
    Boolean bool = CrowCalc.subtractCrows(null);
    System.assertEquals(bool, false, 'subtractCrows return false if argument negative number');
  }

  // (FAILED) negative test -  Exceptions should be handled by the method "subtractCrows
  @isTest
  public static void testSubtractCrowsWithNegativeNumber() {
    Exception unexpectedException;
    try {
      Boolean bool = CrowCalc.subtractCrows(-8);
    } catch (Exception e) {
      unexpectedException = e;
    }
    system.assertEquals(null, unexpectedException, 'Exceptions should be handled by the method "subtractCrows"');
  }

  // (FAILED) negative test -  Exceptions should be handled by the method "AddCrows
  @isTest
  public static void testAddCrowsWithBigNumber() {
    Exception unexpectedException;
    try {
      Boolean addCrowsResult = CrowCalc.addCrows(10002);
      System.assertNotEquals(addCrowsResult, true, 'addCrows return false if argument is big number');
    } catch (Exception e) {
      unexpectedException = e;
    }
    system.assertEquals(null, unexpectedException, 'Exceptions should be handled by the method "AddCrows"');
  }
}
